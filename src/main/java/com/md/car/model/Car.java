package com.md.car.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@ApiModel(value = "Car", description = "Car implementation")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "CAR")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ApiModelProperty(name = "Brand", notes = "Car Brands like Honda, Volkswagen, BMW, Mercedes etc.")
    @Column(name = "brand", nullable = false)
    private String brand;

    @ApiModelProperty(name = "Model", notes = "Car Models like Golf, Civic, Mokka, Elemento etc.")
    @Column(name = "model", nullable = false)
    private String model;

    @ApiModelProperty(name = "Type", notes = "Car Type Enums like SPORTS, SEDAN, SUV, COUPE etc.")
    @Column(name = "type")
    private CarType carType;

    @ApiModelProperty(name = "County", notes = "Car Countries like Germany, Italy, Japan, USA etc.")
    @Column(name = "county")
    private String county;

    @ApiModelProperty(name = "Price", notes = "Active price of a car")
    @Column(name = "price")
    private BigDecimal activePrice;

}
