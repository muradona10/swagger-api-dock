package com.md.car.model;

public enum CarType {

    SEDAN("Sedan"), COUPE("Coupe"), SPORT("Sport"), HATCHBACK("Hatchback"), SUV("SUV");

    private String type;

    CarType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type){
        this.type = type;
    }


}
