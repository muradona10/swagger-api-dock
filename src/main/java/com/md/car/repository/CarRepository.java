package com.md.car.repository;

import com.md.car.model.Car;
import com.md.car.model.CarType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    List<Car> findByBrand(String brand);

    Car findByBrandAndModel(String brand, String model);

    List<Car> findByCarType(CarType carType);

}
