package com.md.car.service;

import com.md.car.model.Car;
import com.md.car.model.CarType;
import com.md.car.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<Car>();
        carRepository.findAll().forEach(cars::add);
        return cars;
    }

    public List<Car> getCarsByBrand(String brand) {
        return carRepository.findByBrand(brand);
    }

    @Override
    public Car getCarByBrandAndModel(String brand, String model) {
        return carRepository.findByBrandAndModel(brand, model);
    }

    public List<Car> getCarByType(String carType) {
        CarType myType = CarType.valueOf(carType);
        return carRepository.findByCarType(myType);
    }

    @Override
    public Car saveCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car updateCar(String brand, String model, Car modifiedCar) {
        Car car = carRepository.findByBrandAndModel(brand, model);
        if (car != null){
            modifiedCar.setId(car.getId());
            return carRepository.save(modifiedCar);
        }
        return null;
    }

    @Override
    public Car deleteCar(String brand, String model) {
        Car car = carRepository.findByBrandAndModel(brand, model);
        if (car != null){
            carRepository.delete(car);
            return car;
        }
        return null;
    }


    @PostConstruct
    private void initData() {
        Car golf = Car.builder()
                .brand("Volkswagen")
                .model("Golf")
                .carType(CarType.HATCHBACK)
                .county("Germany")
                .activePrice(BigDecimal.valueOf(200_000))
                .build();
        carRepository.save(golf);

        Car polo = Car.builder()
                .brand("Volkswagen")
                .model("Polo")
                .carType(CarType.HATCHBACK)
                .county("Germany")
                .activePrice(BigDecimal.valueOf(120_000))
                .build();
        carRepository.save(polo);

        Car civic = Car.builder()
                .brand("Honda")
                .model("Civic")
                .carType(CarType.SEDAN)
                .county("Japan")
                .activePrice(BigDecimal.valueOf(150_000))
                .build();
        carRepository.save(civic);

        Car renegade = Car.builder()
                .brand("Jeep")
                .model("Renegade")
                .carType(CarType.SUV)
                .county("Italy")
                .activePrice(BigDecimal.valueOf(250_000))
                .build();
        carRepository.save(renegade);

        Car i8 = Car.builder()
                .brand("BMW")
                .model("i8")
                .carType(CarType.SPORT)
                .county("Germany")
                .activePrice(BigDecimal.valueOf(950_000))
                .build();
        carRepository.save(i8);

        Car tiguan = Car.builder()
                .brand("Volkswagen")
                .model("Tiguan")
                .carType(CarType.SUV)
                .county("Germany")
                .activePrice(BigDecimal.valueOf(350_000))
                .build();
        carRepository.save(tiguan);

    }
}
