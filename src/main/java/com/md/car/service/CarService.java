package com.md.car.service;

import com.md.car.model.Car;
import com.md.car.model.CarType;

import java.util.List;

public interface CarService {

    List<Car> getAllCars();

    List<Car> getCarsByBrand(String brand);

    Car getCarByBrandAndModel(String brand, String model);

    List<Car> getCarByType(String carType);

    Car saveCar(Car car);

    Car updateCar(String brand, String model, Car modifiedCar);

    Car deleteCar(String brand, String model);
}
