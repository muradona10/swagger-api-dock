package com.md.car.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    private static final String API_TITLE = "Cars API";
    private static final String API_DESCRIPTION = "Cars Api to get all cars, cars by brand, cars by type etc.";
    private static final String API_VERSION = "v1";
    private static final String API_OWNER_NAME = "Murat Demir";
    private static final String API_OWNER_EMAIL = "muradona10@gmail.com";

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.md.car.controller"))
                .paths(PathSelectors.ant("/api/v1/**"))
                .build()
                .apiInfo(getApiInfo());
    };

    private ApiInfo getApiInfo(){
        return new ApiInfoBuilder().title(API_TITLE)
                    .description(API_DESCRIPTION)
                    .version(API_VERSION)
                    .contact(new Contact(API_OWNER_NAME, "",API_OWNER_EMAIL))
                    .license("")
                    .licenseUrl("")
                    .build();
    }

}
