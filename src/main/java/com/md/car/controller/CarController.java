package com.md.car.controller;

import com.md.car.model.Car;
import com.md.car.model.CarListResponse;
import com.md.car.service.CarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Api(value = "Cars", description = "Car REST API", tags = {"Car"})
@RestController
@RequestMapping("/api/v1")
public class CarController {

    @Autowired
    private CarService carService;

    @ApiOperation(notes = "Get all car from api without any input parameter"
            , response = CarListResponse.class
            , value = "All Cars")
    @GetMapping("/cars")
    public ResponseEntity<CarListResponse> getAllCars() {
        List<Car> cars = carService.getAllCars();
        return getCarListResponseResponseEntity(cars);
    }

    @ApiOperation(notes = "Get cars from api with only give brand as an input parameter"
            , response = CarListResponse.class
            , value = "Cars by Brand")
    @GetMapping("/cars/brand/{brand}")
    public ResponseEntity<CarListResponse> gerCarsByBrand(
            @ApiParam(value = "brand name", required = true)
            @PathVariable("brand") String brand) {
        List<Car> cars = carService.getCarsByBrand(brand);
        return getCarListResponseResponseEntity(cars);
    }

    @ApiOperation(notes = "Get a car from api with give brand and model as an input parameters"
            , response = Car.class
            , value = "Car by Brand and Model")
    @GetMapping("/cars/brand/{brand}/model/{model}")
    public ResponseEntity<Car> gerCarByBrandAndModel(
            @ApiParam(value = "brand name", required = true)
            @PathVariable("brand") String brand,
            @ApiParam(value = "model name", required = true)
            @PathVariable("model") String model) {
        Car car = carService.getCarByBrandAndModel(brand, model);
        return getCarResponseEntity(car);
    }

    @ApiOperation(notes = "Get car list from api with give car type as an input parameter"
            , response = CarListResponse.class
            , value = "Cars by Type")
    @GetMapping("/cars/type/{carType}")
    public ResponseEntity<CarListResponse> gerCarByBrandAndModel(
            @ApiParam(value = "car type", required = true)
            @PathVariable("carType") String carType) {
        List<Car> cars = carService.getCarByType(carType);
        return getCarListResponseResponseEntity(cars);
    }

    @ApiOperation(notes = "Save a car on database"
            , response = Car.class
            , value = "Save a car")
    @PostMapping("/cars")
    @ResponseBody
    public ResponseEntity<Car> saveCar(@ApiParam(value = "Car Entity", required = true)
                                       @RequestBody Car car){
        Car savedCar = null;
        if (car != null) {
            savedCar = carService.saveCar(car);
        }
        return getCarResponseEntity(savedCar);
    }

    @ApiOperation(notes = "Update a car on database"
            , response = Car.class
            , value = "Update a car")
    @PostMapping("/cars/{brand}/{model}")
    @ResponseBody
    public ResponseEntity<Car> updateCar(
            @ApiParam(value = "Car Brand") @PathVariable("brand") String brand,
            @ApiParam(value = "Car Model") @PathVariable("model") String model,
            @ApiParam(value = "New Car") @RequestBody Car modifiedCar){
        Car updatedCar = carService.updateCar(brand, model, modifiedCar);
        return getCarResponseEntity(updatedCar);
    }

    @ApiOperation(notes = "Delete car from database"
            , response = Car.class
            , value = "Delete car")
    @DeleteMapping("/cars/{brand}/{model}")
    @ResponseBody
    public ResponseEntity<Car> deleteCar(
            @ApiParam(value = "Car Brand") @PathVariable("brand") String brand,
            @ApiParam(value = "Car Model") @PathVariable("model") String model){
        Car deletedCar = carService.deleteCar(brand, model);
        return getCarResponseEntity(deletedCar);
    }


    private ResponseEntity<CarListResponse> getCarListResponse(List<Car> cars) {
        CarListResponse carListResponse = new CarListResponse(cars);
        return ResponseEntity.ok(carListResponse);
    }

    private ResponseEntity<CarListResponse> getCarListResponseResponseEntity(List<Car> cars) {
        if (cars != null && !CollectionUtils.isEmpty(cars)) {
            return getCarListResponse(cars);
        } else {
            return getCarListResponse(Collections.EMPTY_LIST);
        }
    }

    private ResponseEntity<Car> getCarResponseEntity(Car car) {
        if (car != null) {
            return ResponseEntity.ok(car);
        } else {
            return ResponseEntity.ok(null);
        }
    }

}
